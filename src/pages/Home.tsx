import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  IonButton,
} from "@ionic/react";
import "./Home.css";

const Home: React.FC = () => {

  const ripple = () => {
    
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Blank</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <div
          className=""
          style={{
            background: "#fff",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
            height: "100%",
            flexDirection: "column"
          }}
        >
          <IonButton color="primary">IONIC</IonButton>
          <div className="normal">DIV</div>
          <div className="custom ripple">CUSTOMIZED DIV</div>
        </div>

        <div className="circle ripple">
          <p>START</p>
        </div>


      </IonContent>
    </IonPage>
  );
};

export default Home;

