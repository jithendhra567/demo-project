import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.ionic.sample',
  appName: 'IONIC',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
